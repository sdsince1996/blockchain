const { GENESIS_DATA, MINE_RATE } = require('./config');
const cryptoHash = require('./crypto-hash');
const hexToBinary = require('hex-to-binary');


class Block {
    constructor({ timeStamp, prevHash, difficulty, nonce, data, hash }) {
        this.timeStamp = timeStamp;
        this.prevHash = prevHash;
        this.difficulty = difficulty;
        this.nonce = nonce;
        this.data = data;
        this.hash = hash;
    }
    static Genesis() {
        return new this(GENESIS_DATA);
    }
    static mineBlock({ prevBlock, data }) {
        let hash, timeStamp;
        const prevHash = prevBlock.hash;
        let {difficulty} = prevBlock;

        let nonce = 0;
        do {
            nonce++;
            timeStamp = Date.now();
            difficulty = Block.adjustDifficulty({originalBlock: prevBlock, timeStamp});
            hash = cryptoHash(timeStamp, prevHash, difficulty, nonce, data);
        } while (hexToBinary(hash).substring(0, difficulty) !== '0'.repeat(difficulty))

        return new this({
            timeStamp, 
            prevHash, 
            difficulty, 
            nonce, 
            data, 
            hash
        });
    }
    static adjustDifficulty({originalBlock, timeStamp}){
        const {difficulty} = originalBlock;
        if(difficulty < 1) return 1;
        const difference = timeStamp - originalBlock.timeStamp;
        if(difference > MINE_RATE) return difficulty-1;
        return difficulty+1;
    }
}


module.exports = Block;