const MINE_RATE = 1000;
const INITIAL_DIFFICULTY = 2;
const GENESIS_DATA = {
    timeStamp: 23/02/2022,
    prevHash: '0x000',
    difficulty: INITIAL_DIFFICULTY,
    nonce: 0,
    data: [],
    hash: '0x123',
};

module.exports = {GENESIS_DATA, MINE_RATE};