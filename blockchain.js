const Block = require('./block');
const cryptoHash = require('./crypto-hash');

class BlockChain {
    constructor(){
        this.chain = [Block.Genesis()]
    }

    addBlock({data})
    {
        const newBlock = Block.mineBlock({
            prevBlock: this.chain[this.chain.length - 1],
            data,
        });
        this.chain.push(newBlock);
    }

    replaceChain(chain){
        if(chain.length <= this.chain.length)
        {
            console.error("the incoming chain is not longer");
            return;
        }
        if(!BlockChain.isValidChain(chain)){
            console.error("the incoming chain is not valid");
            return;
        }
        this.chain = chain;
    }

    static isValidChain(chain){
        if(JSON.stringify(chain[0]) !== JSON.stringify(Block.Genesis())) 
        {
            return false;
        }

        for(let i = 1; i < chain.length; i++) 
        {
            const {timeStamp, prevHash,difficulty, nonce, data, hash} = chain[i];
            const realLastHash = chain[i - 1].hash;
            const lastDifficulty = chain[i - 1].difficulty;
            if(prevHash !== realLastHash) return false;

            const validateHash = cryptoHash(timeStamp, prevHash,difficulty, nonce, data);

            if(hash !== validateHash) return false;
            if(Math.abs(lastDifficulty - difficulty) > 1) return false;
        }
        return true;
    }
}

const blockChain = new BlockChain();
blockChain.addBlock({data: "Block3"});
const result = BlockChain.isValidChain(blockChain.chain);
console.log(result);
console.log(blockChain.chain);
module.exports = BlockChain;